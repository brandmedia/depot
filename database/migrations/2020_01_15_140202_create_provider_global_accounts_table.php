<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProviderGlobalAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provider_global_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedDecimal('chiffre_affaire');
            $table->unsignedInteger('qt_35');
            $table->unsignedInteger('qt_12');
            $table->unsignedInteger('qt_6');
            $table->unsignedInteger('qt_3');
            $table->decimal('sold');
            $table->unsignedBigInteger('provider_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provider_global_accounts');
    }
}
