<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('label');
            $table->string('detail');
            $table->string('payment_mode');
            $table->unsignedBigInteger('qt_enter')->nullable();
            $table->unsignedBigInteger('qt_out')->nullable();
            $table->unsignedDecimal('db')->nullable();
            $table->unsignedDecimal('cr')->nullable();
            $table->timestamps();
        });
        Schema::create('global_account', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('account_id')->index();
            $table->unsignedBigInteger('global_id')->index();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
