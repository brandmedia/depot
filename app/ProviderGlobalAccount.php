<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderGlobalAccount extends Model
{
    protected $fillable = ['chiffre_affaire', 'qt_35', 'qt_12', 'qt_6', 'qt_3', 'sold', 'provider_id'];

    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    public function accounts()
    {
        return $this->belongsToMany(Account::class,'global_account','global_id');
    }
}
