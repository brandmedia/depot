<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $fillable = [
        'social_reason', 'name', 'rc', 'patent', 'ice', 'city_id', 'user_id'
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    public function globalAccount()
    {
        return $this->hasOne(ProviderGlobalAccount::class);
    }
}
