<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $timestamps = false;

    protected $fillable = ['city'];

    public function providers()
    {
        return $this->hasMany(City::class);
    }
}
