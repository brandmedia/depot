<?php

namespace App\Http\Controllers\Provider;

use App\Http\Controllers\Controller;
use App\Provider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProviderController extends Controller
{
    public function index()
    {
        return view('provider.index',[
            'providers'     => Provider::all()
        ]);
    }

    public function show(Provider $provider)
    {
        $global = $provider->globalAccount;
        $accounts = $global->accounts;
        return view('provider.show',compact('provider','global', 'accounts'));
    }

    public function store(Request $request)
    {
        $provider = Provider::create([
            'social_reason'     => $request->social_reason,
            'name'              => $request->name,
            'rc'                => $request->rc,
            'patent'            => $request->patent,
            'ice'               => $request->ice,
            'city_id'           => $request->city,
            'user_id'           => auth()->id()
        ]);
        $provider->globalAccount()->create([
            'chiffre_affaire'   => 0,
            'qt_35'             => 0,
            'qt_12'             => 0,
            'qt_6'              => 0,
            'qt_3'              => 0,
            'sold'              => 0,
        ]);
        session()->flash('success', 'Fournisseur a bien été Créer');
        return redirect()->route('provider.index');
    }
}
