<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = ['label', 'detail', 'payment_mode', 'qt_enter', 'qt_out', 'db', 'cr', 'operation_id'];

    public function globals()
    {
        return $this->belongsToMany(ProviderGlobalAccount::class,'global_account','account_id');
    }
}
