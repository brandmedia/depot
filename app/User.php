<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;


    protected $fillable = [
        'name', 'password',
        'first_name', 'last_name',
        'mobile', 'affection','category_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected function getFullNameAttribute()
    {
        return strtoupper($this->last_name) . ' ' . ucfirst($this->first_name);
    }


    public function providers()
    {
        return $this->hasMany(City::class);
    }

}
