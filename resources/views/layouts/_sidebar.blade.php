<section class="main-sidebar">
    <div class="container">
        <div class="user_panel">
            <div class="row">
                <div class="col-sm-12">
                    <div class="info">
                        <label>{{ auth()->user()->full_name }}</label>
                        <label class="online"> <i class="fa fa-circle text-success"></i> online </label>
                    </div>
                </div>
            </div>
        </div>
        <!-- Start Navigation -->
        <ul class="sidebar-menu tree">
            <li class="header">Navigation</li>

            <li class="active">
                <a href="{{ route('home') }}" class="btn bg-gray   text-left">
                    <i class="fa fa-home"></i> <span>Accuiel</span>
                </a>
            </li>
            <li>
                <a href="{{ route('provider.links') }}">
                    <i class="fas fa-truck"></i> <span> Fournisseur </span>
                </a>
            </li>

        </ul>
    </div>
</section>