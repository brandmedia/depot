<!-- header --->
<header class="admin-header">
    <div class="left-part">
        <a href="#" class="logo">
            <span class="logo-mini"></span>
            <span class="logo-lg"><b>Gaz </b> Control Panel </span>
        </a>
    </div>
    <div class="right-part">
        <a href="#" class="float-left to_small_sidebar" id="to_small_sidebar" data-toggle="push-menu" role="button">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </a>

        <div class="dropdown float-right">
            <a role="button" class="dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <img src="{{ asset('images/logo-gaz.png') }}"
                     style="width:25px;border-radius:50%;margin-left: 5px;">
                {{ auth()->user()->full_name }}
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                <a class="dropdown-item" href="#">paramètres  </a>
                <a class="dropdown-item" href="#">mot de passe</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Se déconnecter</a>
            </div>
        </div>
        <div class="dateete">
            @php setlocale(LC_TIME, "fr");@endphp
            {{  Carbon\Carbon::now()->formatLocalized('%d %B %Y') }}
        </div>

    </div>
</header>