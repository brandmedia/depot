@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="page_links">
            <div class="row">
                <div class="col-md-6">
                    <a href="#" > <i class="fas fa-truck-moving"></i> Bon de Commande Fournisseur </a>
                </div>
                <div class="col-md-6">
                    <a href="{{ route('provider.index') }}" ><i class="fas fa-list-ul"></i>  List des Fournisseurs  </a>
                </div>
                <div class="col-md-6">
                    <a href="#" > <i class="fas fa-truck-moving"></i>   Bon de Livraison Fournisseur  </a>
                </div>

                <div class="col-md-6">
                    <a href="#" > <i class="fas fa-user"></i> compte à terme Fournisseur </a>
                </div>
                <div class="col-md-6">
                    <a href="{{ route('provider.create') }}" > <i class="fas fa-user-plus"></i> Ajouter un Fournisseur </a>
                </div>


            </div>
        </div>
    </div>
@endsection
