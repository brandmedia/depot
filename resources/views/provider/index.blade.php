@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-left">Fournisseur</th>
                        <th>Chiffre d'affaire</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($providers as $provider)
                    <tr>
                        <td class="text-left">{{ $provider->name }}</td>
                        <td>{{ $provider->globalAccount->chiffre_affaire }} MAD</td>
                        <td>
                            <a href="#" class="btn btn-success btn-sm"> <i class="fas fa-eye"></i> Compte de Fournisseur</a>
                        </td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection


