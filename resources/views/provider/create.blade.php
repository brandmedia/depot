@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <form method="POST" action="{{ route('provider.store') }}">
                    @csrf
                    <div class="form-group">
                        <label for="social_reason">Raison Sociale</label>
                        <input class="form-control" name="social_reason" id="social_reason" title="Raison Sociale" placeholder="Raison Sociale" required>
                    </div>
                    <div class="form-group">
                        <label for="provider_name">Contact</label>
                        <input class="form-control" name="name" id="provider_name"
                               placeholder="Nom & prenom" required>
                    </div>
                    <div class="form-group">
                        <label for="city">Address</label>
                        <select name="city" id="city" required>
                            <option value="1">Casablanca</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="rc">RC</label>
                        <input class="form-control" name="rc" id="rc" placeholder="RC" required>
                    </div>
                    <div class="form-group">
                        <label for="patent">Patente</label>
                        <input class="form-control" name="patent" id="patent" placeholder="Patente" required>
                    </div>
                    <div class="form-group">
                        <label for="ice">ICE</label>
                        <input class="form-control" placeholder="ICE" name="ice" id="ice" required>
                    </div>
                    <div class="form-group">
                        <input class="btn btn-success" type="submit" value="Ajouter">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
