@extends('layouts.app')

@section('content')
    <div class="container page-cleint-historique">
        <div class="row">
            <div class="col-md-4">
                <table>
                    <tr class="mb-4">
                        <td style="padding: 10px 0;">Compte Fournisseur </td>
                        <td> <span class="span_designed"><b>{{ $provider->id }}</b></span> </td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0;">Raison Sociale </td>
                        <td> <span class="span_designed"><b>{{ $provider->social_reason }}</b></span> </td>
                    </tr>
                    <tr>
                        <td colspan="2"><br><br><br></td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0;">Chiffre d'affaire </td>
                        <td> <span class="span_designed"><b>{{ $global->chiffre_affaire }} MAD</b></span></td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0;">Bénéfice </td>
                        <td> <span class="span_designed"><b>5000,00 MAD</b></span></td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0;">Quantité &nbsp;
                            <select class="btn-spanen">
                                <option value="1">tout</option>
                                <option value="1">35kg</option>
                                <option value="1">12kg</option>
                                <option value="1">6kg</option>
                                <option value="1">3kg</option>
                            </select>
                        </td>
                        <td> <span class="span_designed"><b>150000</b></span></td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0;">Retenue &nbsp;
                            <select class="btn-spanen">
                                <option value="1">tout</option>
                                <option value="1">35kg</option>
                                <option value="1">12kg</option>
                                <option value="1">6kg</option>
                                <option value="1">3kg</option>
                            </select>
                        </td>
                        <td> <span class="span_designed"><b>150000</b></span></td>
                    </tr>

                </table>

            </div>
            <div class="col-md-4">
                <table class="table table-no-border table-date-filter">
                    <tr>
                        <td>De</td>
                        <td><input type="date"></td>
                    </tr>
                    <tr>
                        <td>A</td>
                        <td>
                            <input type="date">
                            <button class="btn-search-filterr" type="button" > <i class="fas fa-search"></i> </button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-4">
                <div class="float-right">
                    <table class="table table-bordered" style="max-width: 150px;">
                        <thead>
                        <tr>
                            <th>QT</th>
                            <th>Remplir</th>
                            <th>Vide</th>
                            <th>Défec</th>
                        </tr>
                        </thead>
                        <tr>
                            <td><b>3KG</b></td>
                            <td><span class="span_designed">1450</span></td>
                            <td><span class="span_designed">0</span></td>
                            <td><span class="span_designed">0</span></td>
                        </tr>
                        <tr>
                            <td><b>6KG</b></td>
                            <td><span class="span_designed">2250</span></td>
                            <td><span class="span_designed">0</span></td>
                            <td><span class="span_designed">0</span></td>
                        </tr>
                        <tr>
                            <td><b>12KG</b></td>
                            <td><span class="span_designed">165</span></td>
                            <td><span class="span_designed">0</span></td>
                            <td><span class="span_designed">0</span></td>
                        </tr>
                        <tr>
                            <td><b>35KG</b></td>
                            <td><span class="span_designed">0</span></td>
                            <td><span class="span_designed">0</span></td>
                            <td><span class="span_designed">225</span></td>
                        </tr>
                    </table>

                </div>

            </div>
        </div>





        <div class="btn btn-lg btn-solde">Solde <span>{{ $global->sold }} MAD</span></div>
        <br>
        <table id="example" class="display dataTables_wrapper" style="width:100%">
            <thead>
            <tr>
                <th>Date</th>
                <th>Libellé</th>
                <th>Détails</th>
                <th>Moyenne de Payement</th>
                <th>QN Sortie</th>
                <th>QN Entre</th>
                <th>Débit</th>
                <th>Crédit</th>
            </tr>
            </thead>
            <tbody>
            @foreach($accounts as $account)
            <tr>
                <td>{{ \Carbon\Carbon::parse($account->created_at)->format('d/m/Y') }}</td>
                <td>{{ $account->label }}</td>
                <td>{{ $account->detail }}</td>
                <td>{{ $account->payment_mode }}</td>
                <td>{{ $account->qt_enter }}</td>
                <td>{{ $account->qt_out }}</td>
                <td>{{ $account->db }}</td>
                <td>{{ $account->cr }}</td>
            </tr>
            @endforeach
            </tbody>

        </table>
    </div>
@endsection



