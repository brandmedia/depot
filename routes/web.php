<?php


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes(['register'    => false]);
Route::middleware('auth')->group(function (){
    Route::view('/','home.home')->name('home');
    // provider
    Route::prefix('provider')->namespace('Provider')->group(function (){
        Route::view('/', 'provider.links')->name('provider.links');
        Route::get('ajouter-un-fournisseur',function(){
            return view('provider.create');
        })->name('provider.create');
        Route::post('ajouter-un-fournisseur','ProviderController@store')
            ->name('provider.store');
        Route::get('liste-des-fournisseurs','ProviderController@index')->name('provider.index');
        Route::get('liste-des-fournisseurs/{provider}','ProviderController@show')->name('provider.show');
    });
/*
    Route::get('fournisseur/bon-commande-fournisseur',function(){
        return view('dashboard.fournisseur.bon_commande_fournisseur');
    })->name('bon_commande_fournisseur');
    Route::get('fournisseur/bon-livraison-fournisseur',function(){
        return view('dashboard.fournisseur.bon_livraison_fournisseur');
    })->name('bon_livraison_fournisseur');

    Route::get('fournisseur/compte-terme-fournisseur',function(){
        return view('dashboard.fournisseur.compte_terme_fournisseur');
    })->name('compte_terme_fournisseur');
    Route::get('fournisseur/historiques-de-fournisseur/1',function(){
        return view('dashboard.fournisseur.historique_fournisseur');
    })->name('historique_fournisseur');
    */
});
